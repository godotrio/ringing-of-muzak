extends "res://addons/top_down_roguelike_toolkit/model/Door.gd"


func _ready():
	$Triggerbox.connect("area_entered", self, "on_area_entered")


func get_dungeon():
	return find_parent("*Dungeon")


# Number of times this door was used.
var usages_count = 0

# This may be triggered again when the character is added to another room,
# if we do not remove the current room from the scene first.
# Hence the thorough debug about usages_count.
func on_area_entered(by_area):
	if not by_area.name == "Actbox":
		print("[%s] Ignoring entering %s area…" % [self.name, by_area.name])
		return
	
	var character = by_area.get_parent()
#	var character = find_character_of_area(by_area)
	
	if not character:
		print("[%s] Ignoring entering %s area because it does not belong to a character…" % [self.name, by_area.name])
		return
	
	if not character.is_controlled_by_local_player:
		print("[%s] Ignoring entering %s area because it does not belong to a controlled character…" % [self.name, by_area.name])
		return
	
	usages_count += 1
	print("[%s] Entered for the %dth time." % [self.name, usages_count])
	
	play_teleport(character)


func play_teleport(character):
	character.can_receive_inputs = false
	character.play_outgoing_teleport()
	yield(get_tree().create_timer(2.0), "timeout")
	get_dungeon().move_party_to_room(to_room_name, to_room_hexagon)
	character.play_incoming_teleport()
	yield(get_tree().create_timer(2.0), "timeout")
	character.can_receive_inputs = true
