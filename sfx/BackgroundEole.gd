extends Node2D

const Cloud = preload("res://sfx/Cloud.tscn")

var b = 0.55

func _ready():
	var ss = OS.get_screen_size()
	for i in range(51):
		var cloud = huff_puff_cloud()
		cloud.position = Vector2(
			rand_range(-b*ss.x, b*ss.x),
			rand_range(-b*ss.y, b*ss.y)
		)

func huff_puff_cloud():
	var ss = OS.get_screen_size()
	var cloud = Cloud.instance()
	add_child(cloud)
	cloud.direction = Vector2(1 if 0==randi()%2 else -1, 0.0)
	cloud.speed = rand_range(0.5, 3.0)
	cloud.flip_h = (0 == randi()%2)
#	cloud.position = Vector2(max(b*ss.x,b*ss.y), 0).rotated(rand_range(0, TAU))
	cloud.position = Vector2(
		b*ss.x * cloud.direction.x * -1,
		rand_range(-b*ss.y, b*ss.y)
	)
	
	cloud.texture = load("res://sprites/clouds/cloud_white_%02d.png" % ( 1 + randi()%8))
	return cloud