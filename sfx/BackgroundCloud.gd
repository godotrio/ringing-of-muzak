extends Sprite


# A cloud is going to drift around slowly in one direction.
export (float) var speed = 1.0
export (Vector2) var direction
# When it gets one screen away from the center, it poofes.  POOF!
# You can also set the deletion boundary by hand.
# We use 0 as default instead of null.
export (float) var deletion_boundary


func _ready():
	var ss = OS.get_screen_size()
	deletion_boundary = pow(max(ss.x, ss.y), 2)
	direction = Vector2(1 if 0==randi()%2 else -1, 0.0)


func _process(delta):
	position += direction * Vector2(rand_range(0, 0.111)*speed, 1.0)
	
	if position.length_squared() > deletion_boundary:
		queue_free()
