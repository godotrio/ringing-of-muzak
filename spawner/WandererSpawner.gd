extends Node2D

export(int) var amount = 1

func _ready():
	
	for i in range(amount):
		var mob_scene = load("res://character/100.Wonderer/Wonderer.tscn").instance()
		mob_scene.name = "Wonderer%d"%(randi()%100)
		mob_scene.position = self.position + Vector2(32, 16)
		# Parent is not ready yet, best defer the call to add_child
		get_parent().call_deferred("add_child", mob_scene)
		
		mob_scene.connect("ready", self, "disappear")

func disappear():
	queue_free()
