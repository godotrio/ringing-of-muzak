# What is this?

No idea.  Let's try to aim for a non-violent Isaac-like game.

Making new rooms/dungeons/mobs/content should be straightforward. (at least we're trying hard to make it so)

# Help wanted

We're pretty bad with GIMP.  Help!


# How to contribute/play

## I. Download Godot

https://godotengine.org/download/

We're using `3.1.1` and upwards.


## II. Open this project using Godot

Run Godot, pick the directory of this project, it should detect the `project.godot`.


## III. Press F5

Run the default scene with `F5`.  Enjoy the show!


# Credits

This game is libre software.

Some of the art was used without the permission of the authors.
We're taking a EAFP approach here.
If you want your art removed, just say the word.

## We used their art, ideas, and other thingies

We declare that all the people listed here are awesome.

- **Ronald Jenkees**, _powering the jam_
- **Edmund Mc Millen**, _crying all over the place_
- **Kenney**, https://www.kenney.nl/assets/hexagon-pack
- **trexman1234**, https://opengameart.org/content/gui-dialoguemenu-box


## They wrote the core engine (like monkeys)

- **Goutte**
- …


## They designed dungeons, levels, mobs…

- **Goutte**
- …
