tool
extends EditorPlugin


func _enter_tree():
	add_custom_type("TextInterfaceEngine", "ReferenceRect", preload("text_interface_engine.gd"), preload("GodotTIE_icon.png"))


func _exit_tree():
	remove_custom_type("TextInterfaceEngine")
