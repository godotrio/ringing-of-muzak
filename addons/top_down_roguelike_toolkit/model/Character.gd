extends KinematicBody2D


const Item = preload("Item.gd")
const ActiveItem = preload("ActiveItem.gd")


export(bool) var debug_mode = true

export(bool) var is_controlled_by_local_player = false
export(String) var local_player_prefix = "p1"

export(bool) var can_receive_inputs = true
export(bool) var is_invulnerable = false

export(int, FLAGS, "Music", "Greed") var team

export(float) var movement_speed_base = 100.0
export(float) var movement_speed_addifier = 0.0
export(float) var movement_speed_multiplier = 1.0

export(float) var movement_friction_base = 0.16
export(float) var movement_friction_addifier = 0.0
export(float) var movement_friction_multiplier = 1.0

export(int) var maximum_health = 6
export(int) var current_health = 6

export(float) var shot_speed_base = 1.0
export(float) var shot_speed_addifier = 0.0
export(float) var shot_speed_multiplier = 1.0

export(float) var shot_delay_base = 32
export(float) var shot_delay_addifier = 0.0
export(float) var shot_delay_multiplier = 1.0

export(float) var shot_height_base = 1.0
export(float) var shot_height_addifier = 0.0
export(float) var shot_height_multiplier = 1.0

export(float) var shot_range_base = 20
export(float) var shot_range_addifier = 0.0
export(float) var shot_range_multiplier = 1.0

export(float) var shot_damage_base = 1.0
export(float) var shot_damage_addifier = 0.0
export(float) var shot_damage_multiplier = 1.0

export(float) var shot_knockback_base = 100.0
export(float) var shot_knockback_addifier = 0.0
export(float) var shot_knockback_multiplier = 1.0

export(Vector2) var shot_origin = Vector2(0, 0)
export(int) var shot_origin_vertical_offset = 9
export(int) var shot_origin_horizontal_offset = 16

# Must have a Projectile as root node.  Other than that, go bonkers.
export(PackedScene) var shot_scene


export(float) var contact_damage_base = 0.0
export(float) var contact_damage_addifier = 0.0
export(float) var contact_damage_multiplier = 1.0


export(Texture) var walking_up_spritesheet
export(Texture) var walking_down_spritesheet
export(Texture) var walking_right_spritesheet


enum {
	TEAM_MUSIC = 1,
	TEAM_GREED = 2,
}


# We get this from move_and_slide()
var linear_velocity = Vector2(0, 0)


signal ready_started
signal ready_finished
signal body_hurt
signal body_died
signal health_changed
signal projectile_hit
signal contact_hit


# Not sure whether we should use this or an accessor.
# This is probably faster.
onready var body = $Appearance/Body
onready var head = $Appearance/Head
onready var walk_animation_player = $WalkAnimationPlayer
onready var phylactery = $Phylactery


func _ready():
	emit_signal("ready_started", {'character': self})
	ready()
	connect_events()
	emit_signal("ready_finished", {'character': self})


func ready():
	pass  # extension point


var current_frame = 0

# Helps throttle shoots from the shoot delay
var last_shot_framestamp = 0


func _physics_process(delta):
	if self.is_controlled_by_local_player:
		apply_local_player_controls(self.local_player_prefix)
	else:
		apply_ai_controls()
	physics_process(delta)
	self.current_frame += 1


func physics_process(delta):
	pass  # extension point, TBD


func apply_ai_controls():
	pass  # extension point, working pretty well so far


func connect_events():
	$Actbox.connect('area_entered', self, 'on_actbox_area_entered')
	$Hurtbox.connect('area_entered', self, 'on_hurtbox_area_entered')
#	$Hitbox.connect('area_entered', self, 'on_hitbox_area_entered')
	connect("body_hurt", self, "on_body_hurt")
	connect("body_died", self, "on_body_died")
	connect("projectile_hit", self, "on_projectile_hit")
	connect("contact_hit", self, "on_contact_hit")


func get_animation_player():
	return $AnimationPlayer


func get_head():
	return $Appearance/Head


func get_body():
	return $Appearance/Body


func get_current_frame():
	return self.current_frame


func get_team():
	return self.team


func get_current_health():
	return self.current_health


func get_maximum_health():
	return self.maximum_health


func get_movement_speed():
	return (self.movement_speed_base + self.movement_speed_addifier) * self.movement_speed_multiplier


func get_movement_friction():
	return (self.movement_friction_base + self.movement_friction_addifier) * self.movement_friction_multiplier


func get_shot_speed():
	return (self.shot_speed_base + self.shot_speed_addifier) * self.shot_speed_multiplier


func get_shot_height():
	return (self.shot_height_base + self.shot_height_addifier) * self.shot_height_multiplier


func get_shot_range():
	return (self.shot_range_base + self.shot_range_addifier) * self.shot_range_multiplier


func get_shot_delay():
	return (self.shot_delay_base + self.shot_delay_addifier) * self.shot_delay_multiplier


func get_shot_damage():
	return (self.shot_damage_base + self.shot_damage_addifier) * self.shot_damage_multiplier


func get_shot_knockback():
	return (self.shot_knockback_base + self.shot_knockback_addifier) * self.shot_knockback_multiplier


func get_contact_damage():
	return (self.contact_damage_base + self.contact_damage_addifier) * self.contact_damage_multiplier


func shoot_up():
	return shoot(Vector2(0, -1), Vector2(0, -self.shot_origin_vertical_offset))


func shoot_down():
	return shoot(Vector2(0, 1), Vector2(0, self.shot_origin_vertical_offset))


func shoot_left():
	return shoot(Vector2(-1, 0), Vector2(-self.shot_origin_horizontal_offset, 0))


func shoot_right():
	return shoot(Vector2(1, 0), Vector2(self.shot_origin_horizontal_offset, 0))


func shoot(direction, origin_offset=Vector2(0, 0)):
	# Make sure we got what we need from the exported variables
	assert(self.shot_scene)
	if not self.shot_scene:
		return
	
	if not can_shoot():
		return
	
	self.last_shot_framestamp = get_current_frame()
	
	var impulse = direction * get_shot_speed() * 200
	var minimum_impulse_strength = impulse.length() * 0.618
	impulse += self.linear_velocity * 0.618
	if impulse.length() < minimum_impulse_strength:
		impulse = impulse.normalized() * minimum_impulse_strength
	
	var shot = self.shot_scene.instance()
	shot.name = "SongOf%s" % self.name
	shot.emitter = self
	shot.team = self.team
	shot.height = get_shot_height()
	shot.lifetime = get_shot_range()
	shot.damage = get_shot_damage()
	shot.position = self.position + self.shot_origin + origin_offset
	shot.apply_central_impulse(impulse)
	
	get_parent().add_child(shot)


func can_shoot():
	# Throttle shoots using shot delay
	if self.last_shot_framestamp + get_shot_delay() > get_current_frame():
		return false
	return true


func apply_move_intention(move_intention):
	move_intention *= 0.333  # voodoo
	
	var applied_linear_velocity = self.linear_velocity + move_intention
#	applied_linear_velocity = applied_linear_velocity.clamped(get_movement_speed())
	applied_linear_velocity = applied_linear_velocity * (1.0 - self.get_movement_friction())
	self.linear_velocity = move_and_slide(applied_linear_velocity)
	
	var animation_player = get_animation_player()
	
	if move_intention.length_squared() > 0.01:
		# We are trying to move around, let's animate things
		
		if move_intention.x > 0:
			body.texture = walking_right_spritesheet
			body.flip_h = false
			head.look_right()
		elif move_intention.x < 0:
			body.texture = walking_right_spritesheet
			body.flip_h = true
			head.look_left()
		elif move_intention.y < 0:
			body.texture = walking_up_spritesheet
			head.look_up()
		else:
			body.texture = walking_down_spritesheet
			head.look_down()
		
		# HACK FIXME
		if "PickingUpThing" != animation_player.current_animation:
			walk_animation_player.play("Walking")
	
	else:
		stop_walking()


func stop_walking():
	if "Walking" == walk_animation_player.current_animation:
		body.texture = walking_down_spritesheet
		walk_animation_player.seek(0, true)
		walk_animation_player.stop()
		head.look_down()


func apply_local_player_controls(prefix):
	if not can_receive_inputs:
		return
	
	var moving_up = Input.is_action_pressed("%s_move_up" % prefix)
	var moving_down = Input.is_action_pressed("%s_move_down" % prefix)
	var moving_left = Input.is_action_pressed("%s_move_left" % prefix)
	var moving_right = Input.is_action_pressed("%s_move_right" % prefix)
	
	var move_intention = Vector2(0, 0)
	if moving_up:
		move_intention += Vector2(0, -1)
	if moving_down:
		move_intention += Vector2(0, 1)
	if moving_left:
		move_intention += Vector2(-1, 0)
	if moving_right:
		move_intention += Vector2(1, 0)
	
	move_intention = move_intention.normalized()
	move_intention = move_intention * get_movement_speed()
	
	apply_move_intention(move_intention)
	
	var head = get_head()
	
	var shooting_up = Input.is_action_pressed("%s_shoot_up" % prefix)
	var shooting_down = Input.is_action_pressed("%s_shoot_down" % prefix)
	var shooting_left = Input.is_action_pressed("%s_shoot_left" % prefix)
	var shooting_right = Input.is_action_pressed("%s_shoot_right" % prefix)
	
	if can_shoot():
		if shooting_up:
			head.look_up(true)
			shoot_up()
		if shooting_down:
			head.look_down(true)
			shoot_down()
		if shooting_left:
			head.look_left(true)
			shoot_left()
		if shooting_right:
			head.look_right(true)
			shoot_right()
	else:
		if shooting_up:
			head.look_up()
		if shooting_down:
			head.look_down()
		if shooting_left:
			head.look_left()
		if shooting_right:
			head.look_right()


func add_health(amount):
	var previous_health = self.current_health
	self.current_health = min(self.maximum_health, max(0, self.current_health + amount))
	emit_signal('health_changed', {
		'character': self,  # we could instead curry the connect() for this
		'previous_health': previous_health,
		'current_health': self.current_health,
		'maximum_health': self.maximum_health,
	})
	if 0 == self.current_health:
		emit_signal("body_died", {})


func remove_health(amount):
	return add_health(-1.0 * amount)


func show_hurting():
	blink_with_color(Color("#FF3399"))


func disable_hitbox():
	$Hitbox.call_deferred('set_monitorable', false)
	$Hitbox.call_deferred('set_visible', false)


func disable_hurtbox():
	$Hurtbox.call_deferred('set_monitoring', false)
	$Hurtbox.call_deferred('set_visible', false)


func disable_actbox():
	$Actbox.call_deferred('set_monitoring', false)
	$Actbox.call_deferred('set_monitorable', false)
	$Actbox.call_deferred('set_visible', false)


func trigger_knockback(from_position, strength):
	#print("[%s] Suffer knockback of %f." % [self.name, strength])
	var direction = self.position - from_position
	direction = direction.normalized()
	self.linear_velocity += direction * strength


var is_dying = false

func is_dying():
	return self.is_dying


func blink_with_color(color):
	var blink_animation = get_animation_player().get_animation("BlinkRed")
#	blink_animation.track_set_key_value(0, 0, Color(0,0,1))
#	prints('Track Value 0 0 :', blink_animation.track_get_key_value(0, 0))
	blink_animation.track_set_key_value(0, 0, {
		'args': [color],
		'method': 'enable_flat_color',
	})
	blink_animation.track_set_key_value(1, 0, {
		'args': [color],
		'method': 'enable_flat_color',
	})
	
	get_animation_player().play("BlinkRed") # fixme
	
	# Old things.  Honestly, use an AnimationPlayer instead.
	var duration = 0.25
	$Tween.interpolate_property(get_body(), "modulate",
		Color(1, 1, 1, 1),
		color,
		duration/2.0,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT,
		0.0  # delay
	)
	$Tween.interpolate_property(get_body(), "modulate",
		color,
		Color(1, 1, 1, 1),
		duration/2.0,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT,
		duration/2.0  # delay
	)
	$Tween.interpolate_property(get_head(), "modulate",
		Color(1, 1, 1, 1),
		color,
		duration/2.0,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT,
		0.0  # delay
	)
	$Tween.interpolate_property(get_head(), "modulate",
		color,
		Color(1, 1, 1, 1),
		duration/2.0,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT,
		duration/2.0  # delay
	)
	$Tween.start()


func play_incoming_teleport():
	self.linear_velocity = Vector2(0, 0)
	get_head().look_down()
	get_animation_player().play('IncomingTeleport')


func play_outgoing_teleport():
	self.linear_velocity = Vector2(0, 0)
	get_head().look_down()
	get_animation_player().play('OutgoingTeleport')


func animate_picking_up_thing(sprite_node):
	sprite_node.name = 'PickedUpThing'
	sprite_node.position = Vector2(0, -42)
	add_child(sprite_node)
	get_animation_player().play('PickingUpThing')


func end_animate_picking_up_thing():
	remove_child(get_node('PickedUpThing'))




#func on_body_entered(event):
#	prints("ON BODY ENTERED", event)
#	# Not sure when this'll happen
#   # Let's not use this for now, unless you know what you're doing.


var has_said_ouch_already = false


func on_body_hurt(event):
	if self.debug_mode:
		print("[%s] Hurt for %d" % [self.name, event.damage])
	remove_health(event.damage)
	show_hurting()
	
	if is_controlled_by_local_player and not has_said_ouch_already:
		has_said_ouch_already = true
		phylactery.show_dialog_autohide("OUCH!\nWhy are you like this?")


func on_body_died(event):
	if self.debug_mode:
		print("[%s] Died!" % [self.name])
	
	self.is_dying = true
	self.is_invulnerable = true
	disable_hitbox()
	disable_hurtbox()
	disable_actbox()
	
	# All the code below should go into children classes instead, pmrobably
	if self.team & TEAM_GREED:
		get_head().set_visible(true)
		get_head().look_down()
		get_head().show_as_shooting()
		apply_move_intention(Vector2(0,0))
		self.phylactery.show_dialog_autohide("Thank you!\nI can now enjoy the jam as well!")
		yield(self.phylactery, 'phylactery_hid')
		get_animation_player().play('OutgoingTeleport')
		yield(get_animation_player(), 'animation_finished')
		queue_free()
	
	if self.is_controlled_by_local_player:
		self.can_receive_inputs = false
		self.phylactery.show_dialog("I'm out of juice… My jam ends here…\n…\n…\n…\n…\n…\n…\n…\n...\n  GAME OVER")
		
		#get_animation_player().play('Defeat')
		self.get_head().set_visible(false)
		$Sfx/ShardsOfLight.restart()
		$Sfx/ShardsOfLight.emitting = true
		
		return
	
	#show_dying()
	queue_free()


func on_projectile_hit(hit_event):
	if self.debug_mode:
		print("[%s] Hit by projectile `%s`." % [self.name, hit_event.projectile.name])
	var projectile = hit_event.projectile
	
	var knockback_strength = 100
	if projectile.emitter:
		knockback_strength = projectile.emitter.get_shot_knockback()
#	knockback_strength += max(0, projectile.linear_velocity.dot((self.position - projectile.position).normalized()))
	
	trigger_knockback(projectile.position, knockback_strength)
	emit_signal("body_hurt", {
		'damage': projectile.get_projectile_damage(),
	})


func on_contact_hit(hit_event):
	var damage = hit_event.hitter.get_contact_damage()
	if damage > 0:
		trigger_knockback(hit_event.hitter.position, 350)
		emit_signal("body_hurt", {
			'damage': damage,
		})
	else:
		if self.debug_mode:
			print("[%s] No damage dealt by %s." % [self.name, hit_event.hitter.name])

#func on_hitbox_body_entered(event):
#	prints("ON HITBOX BODY ENETERD", event)
#
#func on_hitbox_area_entered(by_area):
#	prints("ON HITBOX AREA ENTERED BY", by_area.name)
#
#	if by_area.name.begins_with("Hurtbox"):
#		var hitter = by_area.get_parent()
#		emit_signal("contact_hit", {
#			'hitter': hitter
#		})


func on_hurtbox_area_entered(by_area):
	if self.is_invulnerable:
		return
	
	if not by_area.name.begins_with("Hitbox"):
		return
	
	var hitter = by_area.get_parent()  # caveat: Hitboxes deeper into the scene
	
	if self.debug_mode:
		prints("[%s] Hurtbox entered by area %s of %s." % [self.name, by_area.name, hitter.name])
	
	if hitter.has_method('get_team'):
		if self.get_team() & hitter.get_team():
			return
	
	if hitter.has_method("get_contact_damage"):
		emit_signal("contact_hit", {
			'hitter': hitter,
		})
	elif hitter.has_method("get_projectile_damage"):
		emit_signal("projectile_hit", {
			# Passing the hitter here is bad since we're going to queue_free it.
			# But we're delaying the queue_free, so it works.  Use a pickle?
			'projectile': hitter,
		})
		hitter.emit_signal("enemy_hit", {
			'enemy': self,
		})


var held_items = Array()

func on_actbox_area_entered(by_area):
	if not by_area.name.begins_with("Actbox"):
		return
	
	var target = by_area.get_parent()  # caveat: Actboxes deeper into the scene
	
	if self.debug_mode:
		prints("[%s] Actbox entered by area %s of %s." % [self.name, by_area.name, target.name])
	
	if target is Item:
		target.emit_signal("item_picked_up", {
			'actor': self,
		})
		if target is ActiveItem:
			pass
		else:
			held_items.push_back(target)
		
		animate_picking_up_thing(target.get_node("Icon").duplicate())

