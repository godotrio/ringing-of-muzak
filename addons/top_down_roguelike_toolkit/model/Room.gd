extends Node2D

func get_things_bag():
	return find_node("Things")

func welcome_character(character_scene, starting_hexagon):
	var room_things = get_things_bag()
	room_things.add_child(character_scene)
	
	character_scene.position = $Ground.map_to_world(starting_hexagon) + $Ground.cell_size * 0.5
	#character_scene.play_incoming_teleport()