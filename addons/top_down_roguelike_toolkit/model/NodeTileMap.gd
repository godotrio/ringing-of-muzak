# This does not require being a tool.  Perhaps it will at some point?
#tool
extends TileMap

# The scene this helper node will instantiate and position adequately
# in the center of the tile|cell, for each tile|cell painted, whatever is painted. 
export(PackedScene) var scene_to_instantiate

# The name of the node that will receive the generated nodes.
# Leave empty for this node to be selected as container.
# The container must be a sibling or any (grand)*child of a sibling of this NodeTileMap.
# If you need something more complex, or an absolute node path, just make a MR for it ^u^
export(String) var nodes_container_name = ""

