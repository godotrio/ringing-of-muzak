extends RigidBody2D


signal item_picked_up
signal item_dropped


func _ready():
	ready()


func ready():
	connect_signals()


func get_item_name():
	var file_name = get_script().get_path().get_file()
	return file_name.left(file_name.length()-3)


func connect_signals():
	connect("item_picked_up", self, "on_item_picked_up")
	connect("item_dropped", self, "on_item_dropped")


func on_item_picked_up(event):
	get_parent().remove_child(self)
#	queue_free()


func on_item_dropped(event):
	pass


