extends Node2D

# A Dungeon is a collection of rooms.
# There may be doors between these rooms.
# A Dungeon must have a starting point.
# A Dungeon must have a completion condition.
# A Dungeon may have a failure condition.

const Room = preload("Room.gd")

export(int) var starting_room_index = 0
export(Vector2) var starting_hexagon = Vector2(0, 0)


signal dungeon_built
signal dungeon_entered
signal dungeon_completed
signal dungeon_failed
signal dungeon_exited
signal room_entered
signal room_exited


#func _init():
#	initialize()

# We can't do this in the _init because $Rooms is not yet available.
# We don't have to wait for _ready, though.  We do this right after instance()
# See `Game.gd`.
func initialize():
	connect_signals()
	collect_rooms_from_scene()
	generate_rooms()
	enable_room(get_first_room())
	# Really not sure { 'dungeon': self } is good design
	emit_signal("dungeon_built", { 'dungeon': self })


func get_rooms_bag():
	return $Rooms


func connect_signals():
	connect("dungeon_completed", self, "on_dungeon_completed")


#func _ready():
#	print('ready')


func collect_rooms_from_scene():
	"""
	Returns a list of Room scene instances.  The order should always be the same.
	"""
#	var rooms = Array()
	for room in get_rooms_bag().get_children():
		if room is Room:
			add_room(room)
			disable_room(room)
	
	return self
#	return rooms


func add_room(room):
	print("[%s] Adding room %s." % [name, room.name])
	self.rooms.append(room)
#	get_rooms_bag().add_child(room)
	return self


func get_first_room():
	assert(self.rooms.size())
	return self.rooms.front()


func generate_rooms():
	"""
	Create your rooms scenes and add them one-by-one with add_room(room)
	"""
	pass


func get_starting_room():
	return get_rooms()[self.starting_room_index]


func get_starting_hexagon():
	return self.starting_hexagon


var party # injected by Game

var current_party_room

var rooms = Array()


func get_rooms():
	return self.rooms


func disable_room(room):
	room.visible = false
	if room.get_parent():
		room.get_parent().call_deferred('remove_child', room)
	else:
		printerr("[%s] disable_room(): Room %s has no parent." % [name, room.name])


func enable_room(room):
	room.visible = true
	if not room.is_inside_tree():
		get_rooms_bag().call_deferred("add_child", room)


func get_room(room_descriptor):
	if typeof(room_descriptor) == TYPE_INT:
		return get_rooms()[room_descriptor]
	elif typeof(room_descriptor) == TYPE_STRING:
		for room in get_rooms():
			if room.name == room_descriptor:
				return room
	printerr("No room found by name or id `%s`." % room_descriptor)


func move_party_to_room(room_name, room_hexagon):
	var destination_room = get_room(room_name)
	
	for room in self.rooms:
		if room != destination_room:
			disable_room(room)
	enable_room(destination_room)
	
	current_party_room = destination_room
	var lattice = destination_room.find_node("Ground")
	for character in self.party.characters:
		character.visible = false
		print("[%s] Moving %s to %s." % [self.name, character.name, destination_room.name])
		if character.is_inside_tree():
			character.get_parent().call_deferred("remove_child", character)
		
		character.position = lattice.map_to_world(room_hexagon) + lattice.get_cell_size() / 2.0
		destination_room.get_things_bag().call_deferred("add_child", character)
		character.visible = true
	
	# This has to be done before moving the character or the area_entered of the door is hit twice?
#	for room in self.rooms:
#		if room != destination_room:
#			disable_room(room)
#	enable_room(destination_room)


func on_dungeon_completed(event):
	var character = event.character
	play_teleport_victory(character)

func play_teleport_victory(character):
	var where = character.position
	character.can_receive_inputs = false
	character.play_outgoing_teleport()
	yield(get_tree().create_timer(2.0), "timeout")
	
	character.queue_free()
	
	var victory_overlay = preload("res://hud/VictoryScreen.tscn").instance()
	victory_overlay.position = where
	add_child(victory_overlay)
	
	
