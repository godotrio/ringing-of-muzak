extends RigidBody2D

export(bool) var debug_mode = true

# These are injected (set) upon instantiation by the Character
var lifetime = 42  # in frames
var height = 1.0
var damage = 1.0
var emitter  # Character
var team = 0 # bitmask, see core/character.gd


signal body_destroyed
signal enemy_hit

# Internal
var current_frame = 0


func _ready():
	ready()


func ready():
	self.linear_damp = 0.03
	pretend_there_is_a_shot_altitude()
	connect_signals()


func connect_signals():
	connect('body_destroyed', self, 'on_body_destroyed')
	connect('enemy_hit', self, 'on_enemy_hit')


func _physics_process(delta):
	# Guard to simplify destruction animations
	# Perhaps use set_physics_process(false) instead in destroy()?
	if self.is_destroying:
		return
	
	# Projectiles should stay in the air for their lifetime.
	# Currently, they stay a little longer.   But it's okay.
	if self.current_frame > self.lifetime:
		self.linear_damp = min(5.0, self.linear_damp + 0.08)
	
	if should_lose_height():
		lose_height()
	
	if self.height <= 0:
		self.height = 0
		destroy('no_hit')
	
	pretend_there_is_a_shot_altitude()
	self.current_frame += 1


func pretend_there_is_a_shot_altitude():
	$Body.offset.y = -15 * self.height


func should_lose_height():
	return self.linear_velocity.length_squared() < 6000.0


func lose_height():
	self.height -= 0.077


var is_destroying = false
func destroy(why='hit_on_ground'):
	if self.is_destroying:
		return
	self.is_destroying = true
	
	emit_signal("body_destroyed", { 'why': why })


func get_team():
	return self.team


func get_projectile_damage():
	return self.damage


func configure_dispell(dispell_event):
	$Dispell.position.y = $Body.offset.y
	$Dispell.amount = 1 + ( randi() % 3 )


func start_dispell(dispell_event):
	$Dispell.emitting = true
	$Dispell.restart()


func get_dispell_lifetime():
	return $Dispell.lifetime


func hide_projectile(hide_event):
	$Shadow.visible = false
	$Body.visible = false
	#$Hitbox.disabled = true
	$Boundbox.set_deferred('disabled', true)
	$Boundbox.set_deferred('visible', false)
	$Hitbox.set_deferred('visible', false)
	$Hitbox.set_deferred('monitorable', false)


func on_body_destroyed(event):
	hide_projectile(event)
	configure_dispell(event)
	start_dispell(event)
	yield(get_tree().create_timer(get_dispell_lifetime()), "timeout")
	queue_free()


func on_enemy_hit(event):
	destroy('successful_hit_on_enemy')
