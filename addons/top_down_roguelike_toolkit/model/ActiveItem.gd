extends "res://addons/top_down_roguelike_toolkit/model/Item.gd"

export(int) var current_charge = 3
export(int) var maximum_charge = 3


signal item_used


func can_be_used():
	return has_enough_charges()


func has_enough_charges():
	return self.current_charge >= self.maximum_charge


func on_item_used():
	pass
