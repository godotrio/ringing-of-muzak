extends TileMap

# Occurrences per second, in average. Max 60.
export(float) var water_ripples_frequency = 1.0

# World positions of the top-left vertices of the water hexagons (10 o'clock)
var water_cells = Array()

func _ready():
	collect_water_cells()


func _process(delta):
	if rand_range(0.0, 60.0) <= water_ripples_frequency:
		animate_water_cells()


func collect_water_cells():
	var water_tiles_names = [
		'Water',
		'WaterFull',
		'WaterShadow',
	]
	var water_tiles_ids = Array()
	for water_tile_name in water_tiles_names:
		water_tiles_ids.append(tile_set.find_tile_by_name(water_tile_name))
	
	for water_tile_id in water_tiles_ids:
		for cellv in get_used_cells_by_id(water_tile_id):
			water_cells.append(map_to_world(cellv))

const WaterRipple = preload("res://sfx/WaterRipple.tscn")


func animate_water_cells():
	var water_cells_count = water_cells.size()
	if 0 == water_cells_count:
		return
	var cell = water_cells[randi()%water_cells_count]
	var ripple = WaterRipple.instance()
	ripple.position = cell + Vector2(24.0, 0.0) + Vector2(rand_range(0.0,1.0), rand_range(0.0,1.0)) * (cell_size * Vector2(1.0, 0.618) - Vector2(35.0, 0.0))
	add_child(ripple)
	



