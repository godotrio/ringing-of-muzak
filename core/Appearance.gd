extends Node2D

func enable_flat_color(color):
	$Body.enable_flat_color(color)
	$Head.enable_flat_color(color)

func disable_flat_color():
	$Body.disable_flat_color()
	$Head.disable_flat_color()

func play_rainbow_power_up():
	$Body.play_rainbow_power_up()
	#$Head.play_rainbow_power_up()
