extends Node2D

const Party = preload("res://addons/top_down_roguelike_toolkit/model/Party.gd")


var party

func _ready():
	
	var dungeon_scene = load("res://dungeon/FirstDungeon/FirstDungeon.tscn").instance()
	dungeon_scene.initialize()
#	print(dungeon_scene.get_node('Rooms'))
	
	var room_scene = dungeon_scene.get_starting_room()
	dungeon_scene.current_party_room = room_scene
	
#	var rooms_scene = dungeon_scene.get_rooms_bag()  # careful, _ready may not be done
#	var rooms_scenes = rooms_scene.get_children()
#	assert rooms_scenes.size() > 0
#
#	var room_scene = rooms_scenes.front()
	
#	var room_scene = load("res://room/DebugRoom.tscn").instance()
	var room_things = room_scene.get_node("Things")
	
	self.party = Party.new()
	
	var character_scene = load("res://character/001.Muzak/Muzak.tscn").instance()
	room_things.add_child(character_scene)
	room_scene.welcome_character(character_scene, dungeon_scene.get_starting_hexagon())
#	character_scene.enter_room(room_scene)
#	character_scene.position = Vector2(0, -500)
	
	party.add_character(character_scene)
	
	dungeon_scene.party = party
	
#	var mob_scene = load("res://character/100.Wonderer/Wonderer.tscn").instance()
#	mob_scene.position = Vector2(400, -100)
#	room_things.add_child(mob_scene)

	### HACK TO REMOVE
	
	var item_scene = load("res://item/Triangle/Triangle.tscn").instance()
	item_scene.position = Vector2(200, 100)
	room_things.add_child(item_scene)
	
#	item_scene = load("res://item/Triangle/Triangle.tscn").instance()
#	item_scene.position = Vector2(450, -100)
#	room_things.add_child(item_scene)
#
#	item_scene = load("res://item/Triangle/Triangle.tscn").instance()
#	item_scene.position = Vector2(350, -100)
#	room_things.add_child(item_scene)
	
	########
	
	
	## Bind the HUD
	
	var health_bar = find_node("HealthBar")
	character_scene.connect("health_changed", health_bar, "on_health_changed")
	character_scene.connect("ready_finished", health_bar, "on_ready_finished")
	
	
#	add_child(room_scene)
	add_child(dungeon_scene)
	
	character_scene.play_incoming_teleport()


func _unhandled_key_input(event):
	if Input.is_action_just_pressed("app_fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
