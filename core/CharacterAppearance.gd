extends Sprite

# Since we're changing the material's shader params,
# and we don't want to apply it to ALL characters in the room. 
export(bool) var duplicate_material = true

func _ready():
	if duplicate_material:
		material = material.duplicate()

var current_time = 0.0

func _process(delta):
	get_material().set_shader_param("current_time", current_time)
	current_time += delta

func play_rainbow_power_up():
	get_material().set_shader_param("rainbow_power_up_start_time", current_time)

func enable_flat_color(color):
	get_material().set_shader_param("flat_color", color)
	get_material().set_shader_param("enable_flat_color", true)

func disable_flat_color():
	get_material().set_shader_param("enable_flat_color", false)
