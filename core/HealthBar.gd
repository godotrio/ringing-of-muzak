extends Node2D

const HealthPoint = preload("res://hud/HealthPoint.tscn")

func on_ready_finished(event):
	update_hud(
		event.character.get_current_health(),
		event.character.get_maximum_health()
	)

func on_health_changed(event):
	print("[%s] Changed from %d/%d to %d/%d." % [
		self.name,
		event.previous_health, event.maximum_health,
		event.current_health, event.maximum_health,
	])
	
	update_hud(event.current_health, event.maximum_health)

func update_hud(current_health, maximum_health):
	for node in get_children():
		remove_child(node)
		node.queue_free()
	
	for i in range(maximum_health):
		var hp = HealthPoint.instance()
		hp.position += Vector2(i * 24, 0)
		if i >= current_health:
			hp.set_empty_health_point()
		else:
			hp.set_full_health_point()
		add_child(hp)
