extends Panel


export(Color) var text_color = Color(0, 0, 0, 1)
export(float) var text_speed = 13.0  # In characters per second


signal phylactery_showed
signal phylactery_hid


onready var tie = $TextInterfaceEngine


func _ready():
	visible = false


func show_dialog(message):
	self.visible = true
	tie.reset()
	tie.set_color(self.text_color)
	tie.buff_text(message, 0 if 0 == self.text_speed else 1 / self.text_speed)
	tie.buff_silence(1.618)
	tie.set_state(tie.STATE_OUTPUT)
	emit_signal("phylactery_showed")


func hide_dialog():
	self.visible = false
	tie.reset()
	tie.set_state(tie.STATE_WAITING)
	emit_signal("phylactery_hid")


func show_dialog_autohide(message):
	show_dialog(message)
	# If we yield on state_changed, we get a Stack Overflow
	# because the signal state_change is re-emitted during hide_dialog()
	# below and that fucks up our yield, which restarts, hence the infinite loop.
#	yield(tie, "state_change")
	# So we yield on the end of the buffer instead.
	yield(tie, "buff_end")
	hide_dialog()
