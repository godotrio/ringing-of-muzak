extends Node2D

func _ready():
	$Triggerbox.connect("area_entered", self, "on_area_entered")

func get_dungeon():
	return find_parent("*Dungeon")

# Number of times this trigger was used.
var usages_count = 0

# This may be triggered again when the character is added to another room,
# if we do not remove the current room from the scene first.
# Hence the thorough debug about usages_count.
func on_area_entered(by_area):
	if not by_area.name == "Actbox":
		print("[%s] Ignoring entering %s area…" % [self.name, by_area.name])
		return
	
	var character = by_area.get_parent()
#	var character = find_character_of_area(by_area)
	
	if not character:
		print("[%s] Ignoring entering %s area because it does not belong to a character…" % [self.name, by_area.name])
		return
	
	if not character.is_controlled_by_local_player:
		print("[%s] Ignoring entering %s area because it does not belong to a controlled character (%s)…" % [self.name, by_area.name, character.name])
		return
	
	usages_count += 1
	print("[%s] Entered for the %dth time." % [self.name, usages_count])
	
	if 1 == usages_count:
		get_dungeon().emit_signal("dungeon_completed", {'character': character})
	