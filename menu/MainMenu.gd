extends Control


# This scene should be the entry point of the application.
# Look up the scenes in `core/`.  Most of them are meant to be inherited.
# This project is not frozen.  Anything can change.  Go crazy.


export(bool) var auto_start_new_game = false


func _ready():
	if auto_start_new_game:
		start_new_game()
	$AnimationPlayer.play("GlowYellow", -1, 0.25)


func start_new_game():
	get_tree().change_scene("res://core/Game.tscn")


func _on_play_pressed():
	start_new_game()
