extends Button
#extends TextureButton  # the beacons are lit!


func _ready():
	grab_focus()


func _pressed():
	find_parent("*Menu").start_new_game()

