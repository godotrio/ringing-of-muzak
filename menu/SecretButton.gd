extends Button

func _pressed():
	var animation_player = find_parent("*Menu").find_node("AnimationPlayer")
	animation_player.play("ModulateRainbow", -1, 0.15)
