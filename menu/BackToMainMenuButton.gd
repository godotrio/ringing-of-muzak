extends Button

func _pressed():
	var error = get_tree().change_scene("res://menu/MainMenu.tscn")
	if OK != error:
		printerr("Error #%d. Can't go back to main menu." % error)
