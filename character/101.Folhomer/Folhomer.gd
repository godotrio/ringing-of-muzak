extends "res://addons/top_down_roguelike_toolkit/model/Mob.gd"

func apply_ai_controls():
	if is_dying():
		return
	
	var target = get_first_playable_character_found()
	if null != target:
		follow_playable_character(target)
	else:
		move_randomly_around()
#		printerr('[%s] No PC found to follow.' % [self.name])
#		return


func follow_playable_character(target):
	var move_intention = target.position - self.position
#	move_intention *= 0.2
	move_intention = move_intention.normalized()
	move_intention = move_intention * get_movement_speed()
	apply_move_intention(move_intention)


func get_first_playable_character_found():
#	return get_.find_node("Muzak")
	var character = get_parent().get_node("Muzak")
	
	if character and character.is_invulnerable:
		character = null
	
	return character


## TRAIT: WANDERER ###################################################

# Perhaps just put this in parent Mob...
# Traits would be nice here.

var current_move_intention = Vector2(0, 0)
var move_intention_expiration_framestamp = 0

func move_randomly_around():
	# AI: Move randomly around
	if self.current_frame > self.move_intention_expiration_framestamp:
		if current_move_intention == Vector2():  # Vector2.is_empty()  Why?
			var s = self.get_movement_speed() * 1.618
			self.current_move_intention = Vector2(
				rand_range(-s, s),
				rand_range(-s, s)
			)
			self.move_intention_expiration_framestamp = self.current_frame + 30 + randi() % 120
		else:
			current_move_intention = Vector2()
			self.move_intention_expiration_framestamp = self.current_frame + 15 + randi() % 15
	
	apply_move_intention(self.current_move_intention)


