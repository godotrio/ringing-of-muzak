extends "res://addons/top_down_roguelike_toolkit/model/Mob.gd"


var current_move_intention = Vector2(0, 0)
var move_intention_expiration_framestamp = 0


func physics_process(delta):
	.physics_process(delta)
	move_randomly_around()

func move_randomly_around():
	# AI: Move randomly around
	if self.current_frame > self.move_intention_expiration_framestamp:
		if current_move_intention == Vector2():  # Vector2.is_empty()  Why?
			var s = self.get_movement_speed() * 1.618
			self.current_move_intention = Vector2(
				rand_range(-s, s),
				rand_range(-s, s)
			)
			self.move_intention_expiration_framestamp = self.current_frame + 30 + randi() % 120
		else:
			current_move_intention = Vector2()
			self.move_intention_expiration_framestamp = self.current_frame + 15 + randi() % 15
	
	apply_move_intention(self.current_move_intention)


