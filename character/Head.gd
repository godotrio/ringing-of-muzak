extends Sprite

var is_shooting = false
var last_shoot_framestamp = 0
var current_frame = 0

func _ready():
	# Since we're changing the material's shader params, we duplicate
	material = material.duplicate()

func _physics_process(delta):
	if self.is_shooting:
		if self.current_frame >= self.last_shoot_framestamp + 10:
			self.is_shooting = false
			self.frame -= 1
	
	self.current_frame += 1

func show_as_shooting():
	self.last_shoot_framestamp = self.current_frame
	self.is_shooting = true
	self.frame += 1

func look_up(shooting=false):
	if self.is_shooting:
		return
	self.frame = 4
	if shooting:
		show_as_shooting()

func look_down(shooting=false):
	if self.is_shooting:
		return
	self.frame = 0
	if shooting:
		show_as_shooting()

func look_left(shooting=false):
	if self.is_shooting:
		return
	self.frame = 6
	if shooting:
		show_as_shooting()

func look_right(shooting=false):
	if self.is_shooting:
		return
	self.frame = 2
	if shooting:
		show_as_shooting()

func enable_flat_color(color):
	get_material().set_shader_param("flat_color", color)
	get_material().set_shader_param("enable_flat_color", true)

func disable_flat_color():
	get_material().set_shader_param("enable_flat_color", false)
