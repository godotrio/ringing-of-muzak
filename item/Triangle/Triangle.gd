extends "res://addons/top_down_roguelike_toolkit/model/Item.gd"

func on_item_picked_up(event):
	event.actor.shot_damage_addifier += 1.0
	.on_item_picked_up(event)


func on_item_dropped(event):
	event.actor.shot_damage_addifier -= 1.0
	.on_item_dropped(event)
