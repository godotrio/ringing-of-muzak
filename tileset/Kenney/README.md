 Original tiles from https://opengameart.org/content/hexagon-tiles-93x
 
 We added our own customizations.
 
 Open the `.tscn`, edit it, then convert it to a TileSet `.tres` using Godot's tool in `Scene > Convert > TileSet`.
