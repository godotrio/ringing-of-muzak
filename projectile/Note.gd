extends "res://addons/top_down_roguelike_toolkit/model/Projectile.gd"

func configure_dispell(dispell_event):
	.configure_dispell(dispell_event)
	if 'successful_hit_on_enemy' == dispell_event.why:
		$Dispell.texture = preload("res://sprites/particles/magic_star_white_8.png")
		$Dispell.hue_variation = 1
		$Dispell.hue_variation_random = 1
		$Dispell.scale_amount = 0.5
		$Dispell.amount = 3 + randi()%7