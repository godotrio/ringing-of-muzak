extends Node2D

onready var particles1 = $JoyParticles1
onready var particles2 = $JoyParticles2

export(float) var horizontal_amplitude = 200.0
export(float) var horizontal_speed = 0.0017
export(float) var vertical_amplitude = 200.0
export(float) var vertical_speed = 0.0017


func _process(delta):
	var t = OS.get_ticks_msec()
	# Time for some thrilling mathematics !
	# https://en.wikipedia.org/wiki/Lemniscate_of_Bernoulli
	var position = Vector2(
		horizontal_amplitude * cos(horizontal_speed * t) / (pow(sin(horizontal_speed * t), 2) + 1),
		vertical_amplitude * cos(vertical_speed * t) * sin(vertical_speed * t) / (pow(sin(vertical_speed * t), 2) + 1)
	)
	particles1.position = position
	particles2.position = position * -1