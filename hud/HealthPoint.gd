extends Node2D

func set_empty_health_point():
	$Empty.visible = true
	$Full.visible = false

func set_full_health_point():
	$Empty.visible = false
	$Full.visible = true